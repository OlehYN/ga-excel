import fs from 'fs';

import { allReport } from './src/lib/reports/all-report';
import { functionDimensionReport } from './src/lib/reports/function-dimension-report';
import { functionReport } from './src/lib/reports/function-report';

const functionReports = functionReport(process.argv[2]);
const functionDimensionReports = functionDimensionReport(process.argv[2]);
const all3Reports = allReport(process.argv[2], 3);
const allReports = allReport(process.argv[2], 20);

functionDimensionReports.forEach(({ name, stream }) => {
  // @ts-ignore
  stream.stream.pipe(
    fs.createWriteStream('./results/functions-dimensions/' + name)
  );
});

functionReports.forEach(({ name, stream }) => {
  // @ts-ignore
  stream.stream.pipe(fs.createWriteStream('./results/functions/' + name));
});

all3Reports.forEach(({ name, stream }) => {
  // @ts-ignore
  stream.stream.pipe(fs.createWriteStream('./results/' + name));
});

allReports.forEach(({ name, stream }) => {
  // @ts-ignore
  stream.stream.pipe(fs.createWriteStream('./results/' + name));
});
