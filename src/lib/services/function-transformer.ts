import _ from 'lodash';

import {
  IterationResult,
  IterationResultStats,
  statsFields
} from '../interfaces/iteration-result.interface';
import {
  fileFields,
  FileKey,
  FileResult
} from '../interfaces/reports/function-report.interface';

import { DIMENSIONS } from '../constants';
import { aggregate } from '../utils/aggregator';
import { avgFieldsAggregator } from '../utils/avg-aggregator';
import { maxFieldsAggregator } from '../utils/max-aggregator';
import { successFilter } from '../utils/success_filter';

class Transformer {
  public getAggregatedIterations(
    iterationResults: IterationResult[],
    iterationFields: string[]
  ): FileResult[] {
    const criteriaFields = _.difference(iterationFields, [
      ...fileFields,
      ...statsFields,
      'dimension',
      'run'
    ]) as Array<
      Exclude<
        keyof IterationResult,
        keyof FileKey | keyof IterationResultStats | 'dimension' | 'run'
      >
    >;

    const fileResults = aggregate<IterationResult, keyof FileKey>(
      iterationResults,
      fileFields
    ).map(({ key, data }) => {
      const result = aggregate(data, criteriaFields);

      return { key, data: result };
    });

    return fileResults.map(({ key, data }) => {
      return {
        key,
        data: data.map(runData => {
          const dimensions = DIMENSIONS.map(value => {
            const dimensionData = runData.data.filter(
              ({ dimension }) => dimension === value
            );

            return [
              value,
              {
                sucRuns:
                  successFilter(dimensionData).length / dimensionData.length,
                avg: avgFieldsAggregator(
                  successFilter(dimensionData),
                  statsFields
                ),
                max: maxFieldsAggregator(
                  successFilter(dimensionData),
                  statsFields
                )
              }
            ];
          });

          const successData = successFilter(runData.data);
          const allThreeDimData = runData.data.filter(
            ({ dimension }) => dimension <= 3
          );
          const sucThreeDimData = successData.filter(
            ({ dimension }) => dimension <= 3
          );

          return {
            key: runData.key,
            all: {
              avg: avgFieldsAggregator(successData, statsFields),
              max: maxFieldsAggregator(successData, statsFields)
            },
            three: {
              avg: avgFieldsAggregator(sucThreeDimData, statsFields),
              max: maxFieldsAggregator(sucThreeDimData, statsFields)
            },
            dimensions: _.fromPairs(dimensions),
            data: runData.data,
            threeSucRuns: sucThreeDimData.length / allThreeDimData.length,
            sucRuns: successData.length / runData.data.length
          };
        })
      };
    });
  }
}

export const functionTransformer = new Transformer();
