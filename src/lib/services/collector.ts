import fs from 'fs';
import path from 'path';

import { IterationResult } from '../interfaces/iteration-result.interface';

class Collector {
  public collect(statsFolder: string): IterationResult[] {
    const files: string[] = fs.readdirSync(statsFolder);
    const iterationResuls = files
      .map(file => fs.readFileSync(path.resolve(statsFolder, file)).toString())
      .map(fileContent => JSON.parse(fileContent));

    return iterationResuls as IterationResult[];
  }
}

export const collector = new Collector();
