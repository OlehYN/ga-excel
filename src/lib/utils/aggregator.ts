import _ from 'lodash';

type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;

export interface Aggregator<T, K extends keyof T> {
  key: Pick<T, K>;
  data: Array<Omit<T, K>>;
}

export const aggregate = <T extends object, K extends keyof T>(
  data: T[],
  fields: K[]
): Array<Aggregator<T, K>> => {
  const aggregateResults: Array<Aggregator<T, K>> = [];

  _.each(data, row => {
    const search = { key: _.pick(row, fields) as Pick<T, K> };
    const aggregateResult = _.find(aggregateResults, search) as Aggregator<
      T,
      K
    >;

    if (aggregateResult) {
      aggregateResult.data.push(_.omit(row, fields) as Omit<T, K>);
    } else {
      const aggregator: Aggregator<T, K> = {
        data: [_.omit(row, fields) as Omit<T, K>],
        key: _.pick(row, fields)
      };
      aggregateResults.push(aggregator);
    }
  });

  return aggregateResults;
};
