import _ from 'lodash';

export const maxAggregator = <T, P extends keyof T>(
  data: T[],
  key: P,
  reverse?: boolean
): T[P] | undefined => {
  return (reverse ? _.min : _.max)(
    data.map(entry => entry[key]).filter(_.isNumber)
  );
};

export const maxFieldsAggregator = <T, P extends keyof T>(
  data: T[],
  keys: P[],
  reverse?: boolean
): Partial<T> => {
  const stats: Partial<T> = {};

  _.forEach(keys, key => {
    stats[key] = maxAggregator(data, key, reverse);
  });

  return stats;
};
