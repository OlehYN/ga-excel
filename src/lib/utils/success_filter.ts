import { MAX_NFE } from './../constants';

export const successFilter = <T extends { nfe: number }>(data: T[]): T[] => {
  return data.filter(value => value && value.nfe < MAX_NFE);
};
