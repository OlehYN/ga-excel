import { Borders, stream } from 'exceljs';
import _ from 'lodash';

import {
  BlockDefinition,
  BlockDefinitionConfig
} from '../excel/block/block-definition.interface';
import { ExcelContentBlockDefinition } from '../excel/block/excel-block-definition.interface';
import { getExcelBlockDefinitions } from '../excel/block/excel-block-transformer';
import { Composer } from '../excel/composer/composer';
import { statsFields } from '../interfaces/iteration-result.interface';
import { fileFields } from '../interfaces/reports/function-report.interface';
import { collector } from '../services/collector';
import { functionTransformer } from '../services/function-transformer';
import { DIMENSIONS } from './../constants';

const border: Borders = {
  top: { style: 'thin' },
  left: { style: 'thin' },
  bottom: { style: 'thin' },
  right: { style: 'thin' },
  diagonal: {}
};

interface Property {
  prop: string;
  key: string;
  config?: BlockDefinitionConfig;
}

const configGenerator: (
  configuration: Property[],
  sections: Property[],
  criteria: Property[]
) => BlockDefinition[] = (configuration, sections, criteria) => [
  {
    value: 'Критерій',
    excelCellConfig: { style: { border } },
    children: configuration.map(({ prop, key: propKey, config }) => ({
      value: prop,
      config,
      excelCellConfig: { style: { border } },
      excelConfig: {
        key: `key.${propKey}`
      }
    }))
  },
  ...sections.map(({ prop: sectionProp, key: sectionKey }) => ({
    value: sectionProp,
    excelCellConfig: { style: { border } },

    children: [
      ...['avg', 'max'].map(name => ({
        value: name,
        excelCellConfig: { style: { border } },

        children: criteria.map(
          ({ prop: criterionProp, key: criterionKey }) => ({
            value: criterionProp,
            excelCellConfig: { style: { border } },
            excelConfig: {
              key: `${sectionKey}.${name}.${criterionKey}`
            }
          })
        )
      })),
      {
        value: 'sucRuns',
        excelCellConfig: { style: { border } },
        excelConfig: { key: `${sectionKey}.sucRuns` }
      }
    ]
  })),
  {
    value: '1-3 Suc Runs',
    excelCellConfig: { style: { border } },
    excelConfig: { key: 'threeSucRuns' }
  },
  {
    value: '1-20 Suc Runs',
    excelCellConfig: { style: { border } },
    excelConfig: { key: 'sucRuns' }
  }
];

const excelOptions = {
  useStyles: true,
  dateFormats: ['DD/MM/YYYY']
};

export interface FunctionReport {
  name: string;
  stream: stream.xlsx.WorkbookWriter;
}

export const functionReport = (path: string): FunctionReport[] => {
  const iterationResults = collector.collect(path);
  const iterationFields = _.keys(_.first(iterationResults));

  const configuration = _.difference(iterationFields, [
    ...fileFields,
    ...statsFields,
    'dimension',
    'run'
  ]).map(name => ({ prop: name, key: name }));

  const criteria = [
    ...statsFields.map(name => ({ prop: name, key: name }))
    // { criterion: 'SucRuns', key: 'sucRuns' }
  ];

  const sections = [
    ...DIMENSIONS.map(value => ({
      prop: `Dim ${value}`,
      key: `dimensions[${value}]`
    })),
    { prop: 'Dim 1-3', key: 'three' },
    { prop: 'Dim 1-20', key: 'all' }
  ];

  const data = functionTransformer.getAggregatedIterations(
    iterationResults,
    iterationFields
  );

  const headers: BlockDefinition[][] = [];
  const footers: BlockDefinition[][] = [];

  const content = getExcelBlockDefinitions(
    configGenerator(configuration, sections, criteria)
  ) as ExcelContentBlockDefinition;

  const arr: FunctionReport[] = data.map(subData => {
    const composer = new Composer(
      'report',
      headers.map(getExcelBlockDefinitions),
      content,
      footers.map(getExcelBlockDefinitions),
      subData.data,
      excelOptions
    );

    return {
      name: `T_${subData.key.evaluation}.xlsx`,
      stream: composer.generate()
    };
  });

  return arr;
};
