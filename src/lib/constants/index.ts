export const MAX_NFE = process.env.MAX_NFE
  ? Number(process.env.MAX_NFE)
  : 20000000;
export const DIMENSIONS = [1, 2, 3, 4, 5, 10, 15, 20];
