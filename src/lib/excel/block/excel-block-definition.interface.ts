import { Cell, Column } from 'exceljs';

export interface BlockRange {
  lineStart: number;
  lineEnd: number;
  columnStart: number;
  columnEnd: number;
}

export interface ExcelBlockCellConfig {
  line: number;
  column: number;
  cellConfig: Partial<Cell>;
}

export interface ExcelBlockDefinition {
  data: Array<Array<string | null | undefined>>;
  getRange: (initRow: number) => BlockRange[];
  getExcelConfigs: (initRow: number) => ExcelBlockCellConfig[];
  excelConfigs?: Array<Partial<Column>>;
}

export interface MandatoryColumnKey extends Partial<Column> {
  key: string;
}

export interface ExcelContentBlockDefinition extends ExcelBlockDefinition {
  excelConfigs: MandatoryColumnKey[];
}
